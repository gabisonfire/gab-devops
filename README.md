# Plotly DevOps Homework Assignment

## TL;DR

Testing
```bash
git clone git@gitlab.com:gabisonfire/gab-devops.git
docker-compose -f tests/docker-compose.yml up -d
```
Deploying
```bash
git clone git@gitlab.com:gabisonfire/gab-devops.git
cd deploy
helm dependency build .
helm install <release-name> .
```

## Local testing

To deploy locally, clone the repository, then execute the following docker-compose command:

```bash
docker-compose -f tests/docker-compose.yml up -d
```

Using `docker ps`, you should be able to see both running containers:
```
docker ps
CONTAINER ID   IMAGE             COMMAND                  CREATED             STATUS             PORTS                                       NAMES
d3ba5fffc70c   mariadb:10.6.13   "docker-entrypoint.s…"   About an hour ago   Up About an hour   0.0.0.0:3306->3306/tcp, :::3306->3306/tcp   tests-mariadb-1
5774988b2717   gab:test          "python app/userapi.…"   About an hour ago   Up About an hour   0.0.0.0:5001->5000/tcp, :::5001->5000/tcp   tests-app-1
```
We now need to initate the DB with the proper columns and constraints:
```bash
./tests/db_init.sh
```

To further test your setup, you can run `./tests/crud-test.sh localhost:5001` to ensure all operations are working properly.

```
Create user:
"User created successfully!"
Create user again, will raise an error
"(1062, \"Duplicate entry 'gab@gab123.com' for key 'unique_email'\")"
Create another user:
"User created successfully!"
Get users
[[1,"Gab","gab@gab123.com","hunter2"],[3,"Frank","franky@frankyfranky.com","hunter2"]]
Get Frank
[3,"Frank","franky@frankyfranky.com","hunter2"]
Update Frank
"User updated successfully!"
Get Frank
[3,"Frank","franky@frankyfranky2.com","hunter123"]
Delete Frank
"User deleted successfully!"
Get users
[[1,"Gab","gab@gab123.com","hunter2"]]
```

## Building the image

The docker image building process is handled by the Gitlab runner and can be triggered by creating a tag. The image will use the set tag as the image's version. See `.gitlab-ci.yml` for more details.

## Deploying using Helm

To deploy using Helm, navigate to the `deploy` directory to review and adjust values in the `values.yaml` file.
Start the installation by running:
```bash
helm dependency build . # Only needed on the first run
helm install <release-name> .
```
```bash
kubectl get pods
NAME                          READY   STATUS    RESTARTS   AGE
app-deploy-5b766dc65d-sk8p8   1/1     Running   0          4m20s
test-mariadb-0                1/1     Running   0          4m19s
```
All done!

### Testing

Testing on the helm deployment can be done using the same `crud-test.sh` script and `kubectl port-forward`
```bash
kubectl port-forward svc/app-service 8080
Forwarding from 127.0.0.1:8080 -> 5000
Forwarding from [::1]:8080 -> 5000
```
And from another terminal
```bash
../tests/crud-test.sh localhost:8080
Create user:
"User created successfully!"
Create user again, will raise an error
"(1062, \"Duplicate entry 'gab@gab123.com' for key 'unique_email'\")"
Create another user:
"User created successfully!"
Get users
[[1,"Gab","gab@gab123.com","hunter2"],[3,"Frank","franky@frankyfranky.com","hunter2"]]
Get Frank
[3,"Frank","franky@frankyfranky.com","hunter2"]
Update Frank
"User updated successfully!"
Get Frank
[3,"Frank","franky@frankyfranky2.com","hunter123"]
Delete Frank
"User deleted successfully!"
Get users
[[1,"Gab","gab@gab123.com","hunter2"]]
```