#!/bin/bash
DB_HOST=$1
USER_JSON=$(cat <<EOF
{
    "name": "Gab",
    "email": "gab@gab123.com",
    "pwd": "hunter2"
}
EOF
)
USER2_JSON=$(cat <<EOF
{
    "name": "Frank",
    "email": "franky@frankyfranky.com",
    "pwd": "hunter2"
}
EOF
)
USER2_JSON_UPDATE=$(cat <<EOF
{
    "name": "Frank",
    "email": "franky@frankyfranky2.com",
    "pwd": "hunter123",
    "user_id": 3
}
EOF
)

echo "Create user:"
curl -XPOST -H "Content-type: application/json" -d "$USER_JSON" $DB_HOST/create
echo "Create user again, will raise an error"
curl -XPOST -H "Content-type: application/json" -d "$USER_JSON" $DB_HOST/create
echo "Create another user:"
curl -XPOST -H "Content-type: application/json" -d "$USER2_JSON" $DB_HOST/create
echo "Get users"
curl $DB_HOST/users
echo "Get Frank"
curl $DB_HOST/user/3
echo "Update Frank"
curl -XPOST -H "Content-type: application/json" -d "$USER2_JSON_UPDATE" $DB_HOST/update
echo "Get Frank"
curl $DB_HOST/user/3
echo "Delete Frank"
curl $DB_HOST/delete/3
echo "Get users"
curl $DB_HOST/users