# App Continuous delivery

## ArgoCD

The application will be deployed as an ArgoCD application. I chose ArgoCD because of it's flexibility, proven track record and overall rise to become an industry standard. It's free and open-source and k-native.

An ArgoCD application manifest for our app would look something like this:

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: app
  labels:
    component: app
spec:
  sources:
  - chart: app
    repoURL: https://our-chart-repo.com/charts
    targetRevision: 0.0.1
    helm:
      valueFiles:
        - $values/deploy/values.yaml
  - repoURL: https://gitlab.com/gabisonfire/gab-devops.git
    targetRevision: HEAD
    ref: values
  destination:
    server: "https://kubernetes.default.svc/"
    namespace: app
  project: default
  syncPolicy:
    syncOptions:
      - CreateNamespace=true
```

## Deployment flow

![flow](flow.png)