FROM python:3.9.16-alpine3.18
COPY app app
RUN apk add --no-cache build-base linux-headers && \
    pip install --no-cache-dir -r app/requirements.txt && \
    apk del linux-headers build-base
CMD ["python", "app/userapi.py"]