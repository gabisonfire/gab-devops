Flask==1.0.3  
Flask-MySQL==1.4.0  
PyMySQL==0.9.3
uWSGI==2.0.17.1
mysql-connector-python
cryptography
jinja2==3.0.3
itsdangerous==2.0.1